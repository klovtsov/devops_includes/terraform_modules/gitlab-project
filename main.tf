terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">=16.5.0"
    }
  }
}

resource "gitlab_project" "projects" {
  for_each = var.projects

  name                                             = each.value.name
  namespace_id                                     = each.value.namespace_id
  allow_merge_on_skipped_pipeline                  = each.value.allow_merge_on_skipped_pipeline
  analytics_access_level                           = each.value.analytics_access_level
  approvals_before_merge                           = each.value.approvals_before_merge
  archive_on_destroy                               = each.value.archive_on_destroy
  archived                                         = each.value.archived
  auto_cancel_pending_pipelines                    = each.value.auto_cancel_pending_pipelines
  auto_devops_deploy_strategy                      = each.value.auto_devops_deploy_strategy
  auto_devops_enabled                              = each.value.auto_devops_enabled
  autoclose_referenced_issues                      = each.value.autoclose_referenced_issues
  avatar                                           = each.value.avatar
  avatar_hash                                      = try(filesha256(each.value.avatar), null)
  build_git_strategy                               = each.value.build_git_strategy
  build_timeout                                    = each.value.build_timeout
  builds_access_level                              = each.value.builds_access_level
  ci_config_path                                   = each.value.ci_config_path
  ci_default_git_depth                             = each.value.ci_default_git_depth
  ci_forward_deployment_enabled                    = each.value.ci_forward_deployment_enabled
  ci_separated_caches                              = each.value.ci_separated_caches
  container_registry_access_level                  = each.value.container_registry_access_level
  default_branch                                   = each.value.default_branch
  description                                      = each.value.description
  emails_disabled                                  = each.value.emails_disabled
  environments_access_level                        = each.value.environments_access_level
  external_authorization_classification_label      = each.value.external_authorization_classification_label
  feature_flags_access_level                       = each.value.feature_flags_access_level
  group_with_project_templates_id                  = each.value.group_with_project_templates_id
  import_url                                       = each.value.import_url
  import_url_username                              = each.value.import_url_username
  import_url_password                              = each.value.import_url_password
  infrastructure_access_level                      = each.value.infrastructure_access_level
  initialize_with_readme                           = each.value.initialize_with_readme
  issues_access_level                              = each.value.issues_access_level
  issues_enabled                                   = each.value.issues_enabled
  issues_template                                  = each.value.issues_template
  keep_latest_artifact                             = each.value.keep_latest_artifact
  lfs_enabled                                      = each.value.lfs_enabled
  merge_commit_template                            = each.value.merge_commit_template
  merge_method                                     = each.value.merge_method
  merge_pipelines_enabled                          = each.value.merge_pipelines_enabled
  merge_requests_access_level                      = each.value.merge_requests_access_level
  merge_requests_enabled                           = each.value.merge_requests_enabled
  merge_requests_template                          = each.value.merge_requests_template
  merge_trains_enabled                             = each.value.merge_trains_enabled
  mirror                                           = each.value.mirror
  mirror_overwrites_diverged_branches              = each.value.mirror_overwrites_diverged_branches
  mirror_trigger_builds                            = each.value.mirror_trigger_builds
  monitor_access_level                             = each.value.monitor_access_level
  only_allow_merge_if_all_discussions_are_resolved = each.value.only_allow_merge_if_all_discussions_are_resolved
  only_allow_merge_if_pipeline_succeeds            = each.value.only_allow_merge_if_pipeline_succeeds
  only_mirror_protected_branches                   = each.value.only_mirror_protected_branches
  packages_enabled                                 = each.value.packages_enabled
  pages_access_level                               = each.value.pages_access_level
  path                                             = each.value.path
  printing_merge_request_link_enabled              = each.value.printing_merge_request_link_enabled
  public_jobs                                      = each.value.public_jobs
  releases_access_level                            = each.value.releases_access_level
  remove_source_branch_after_merge                 = each.value.remove_source_branch_after_merge
  repository_access_level                          = each.value.repository_access_level
  request_access_enabled                           = each.value.request_access_enabled
  requirements_access_level                        = each.value.requirements_access_level
  resolve_outdated_diff_discussions                = each.value.resolve_outdated_diff_discussions
  restrict_user_defined_variables                  = each.value.restrict_user_defined_variables
  security_and_compliance_access_level             = each.value.security_and_compliance_access_level
  shared_runners_enabled                           = each.value.shared_runners_enabled
  snippets_access_level                            = each.value.snippets_access_level
  snippets_enabled                                 = each.value.snippets_enabled
  squash_option                                    = each.value.squash_option
  template_name                                    = each.value.template_name
  template_project_id                              = each.value.template_project_id
  use_custom_template                              = each.value.use_custom_template
  topics                                           = each.value.topics
  visibility_level                                 = each.value.visibility_level
  wiki_access_level                                = each.value.wiki_access_level
  wiki_enabled                                     = each.value.wiki_enabled

  lifecycle {
    ignore_changes = [
      import_url
    ]
  }
}
